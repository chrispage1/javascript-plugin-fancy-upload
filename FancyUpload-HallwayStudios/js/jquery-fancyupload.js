/**
* jQuery Fancy Upload
* @version 0.1
* @author Hallway Studios <technical@hallwaystudios.com>
* @usage $('UL.nav').fancyUpload();
*
* @args defaultText string Text to use when there is no attachment
*/
$.fn.fancyUpload = function(data) {
        
	// generate unique ID for upload box and determine default text to use
	var uploadBox = $(this);
	var uniqID = Math.floor( Math.random() * 999999 );
	var defText = (data == "" || data == undefined || data.defaultText == "" || data.defaultText == undefined) ? 'Click here to add an Attachment' : data.defaultText;
	
	// hide the original upload box and replace with fancyUpload
	uploadBox.hide();
	uploadBox.before('<input class="fancyUpload" type="text" value="' + defText + '" id="uploadID'+uniqID+'" />').wrap('<div />');
	
	var newUploadBox = $('INPUT[type=text]#uploadID'+uniqID);
	
	// handle clicks on new upload box
	newUploadBox.click(function (e) {
		var _this = $(this);
		
		// blur the text box because we dont want to focus on it and emulate click on file upload
		_this.blur().siblings('div:first').children('INPUT[type=file]').click().bind('change', function (e) {
			// determine resulting file name by getting last element in full file path
			var filename = $(this).val().split("\\");
			filename = filename[filename.length-1];
			
			// set file name on emulated text box
			_this.attr({ 'value' : (filename == "" || filename == undefined) ? defText : 'Attachment: ' + filename }).addClass('fileOn');
			
			// handle form field resets (reset to defText)
			_this.parents('FORM:first').find('INPUT[type=reset]').click(function () {
				_this.attr({ 'value' : defText }).removeClass('fileOn');
			});
		});
	});
};