![Hallway Studios](http://email.hallwaystudios.com/logo-height80.gif "Hallway Studios")

# Fancy Upload jQuery Plugin

This is a simple *jQuery* plugin available in both compressed and uncompressed formats. This restyles ugly upload fields to 
a nicely formatted text box. The styling is completely flexible. This plugin is great from a usability stance and makes your 
upload fields look much nicer!

## Usage

The usage is simple and only requires you to call the fancyUpload() plugin against the file upload element, e.g. ``$('INPUT[type=file]').fancyUpload();``.

The CSS class used is ``fancyUpload`` and when a file has been selected, a ``fileOn`` class will be added for additional styling.

### Paramaters

The following paramaters can be used for this plugin:
``defaultText`` - Specifies a text to use other than  the default 'Click here to add an Attachment'